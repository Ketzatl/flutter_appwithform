import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:adopteunalternant/screens/Guest.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Firebase.initializeApp();

  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'adopteunalternant.com',
      home: GuestScreen(),
    );
  }
}
